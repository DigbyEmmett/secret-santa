const express = require("express");
const hb = require("express-handlebars")
const waitUntil = require("async-wait-until")
const session = require("express-session")
const bodyP = require("body-parser")
const uuidv1 = require("uuid/v4");
var sqlite = require("sqlite3").verbose();
const path = require("path");
const app = express();

app.use(session({
    secret:"secret dont tell",
    cookie:{secure:false},
    resave:true,
    rolling:true,
     
}))
app.engine('handlebars',hb({defaultLayout:null}))
app.set("view engine",'handlebars');
app.use(express.static("public"))
app.use(bodyP.urlencoded({extended:true}))


var db = new sqlite.Database("data/pokemon.db",
    function(err){
        if(err) {
            throw err;
        }
    }
)
db.serialize(function() {
db.run( "CREATE TABLE IF NOT EXISTS USERS " +
        "(UUID STRING PRIMARY KEY, EMAIL STRING, NAME STRING, PASS STRING,UNIQUE(EMAIL))")
db.run("CREATE TABLE IF NOT EXISTS GROUPS (UGID STRING PRIMARY KEY, NAME STRING, EXP INT, DRAWN INTEGER)")
db.run("CREATE TABLE IF NOT EXISTS USERGROUPS (UGID STRING, UUID STRING, WLST STRING, SOF STRING)")

db.each("SELECT * FROM GROUPS WHERE DRAWN=0",[],(err,row)=>{
        if(err)
            throw err;
        waitUntil(function() {
                const time = new Date(row.EXP) - new Date();
                return time<=0?true:false; 
            }).then(
            function (passed) {
            console.log("Assigning Santas")
            //here we randomly allocate the people to the other people
            //first get the people in this group
            db.run(`UPDATE GROUPS SET DRAWN=1 WHERE UGID='${row.UGID}'`);
            db.all(`SELECT USERS.* FROM USERS,USERGROUPS,GROUPS WHERE USERS.UUID=USERGROUPS.UUID AND ` + 
            `USERGROUPS.UGID=GROUPS.UGID AND GROUPS.UGID='${row.UGID}'`,
            [],function(err,rows) {
                if(err)
                    throw err;
                var swapUUID;
                var to, from;
                from = Math.floor(Math.random()*rows.length) 
                to = from;
                while(to==from)
                    to = Math.floor(Math.random()*i);
                db.run(`UPDATE USERGROUPS SET SOF='${rows[to].UUID}' WHERE ` +
                       `UUID='${rows[from].UUID}'`);
                swapUUID = rows[from].UUID
                from = to;
                for(i=rows.length-1;i>0;i++) {
                    while(to==from)
                        to = Math.floor(Math.random()*i);
                    swapGUID = rows[from].UUID;
                    db.run(`UPDATE USERGROUPS SET SOF='${rows[to].UUID}' WHERE ` +
                           `UUID='${swapUUID}'`);
                    rows.splice(from,1);
                     
                    from = to;
                }
                });
            });
        });
});
app.get("/",(req,res)=> {
        res.render(path.join(__dirname,"views","index"));
});

app.get("/user",(req,res) => {
    if(req.session.user) {
    let query = `SELECT GROUPS.NAME,GROUPS.EXP,GROUPS.UGID,` + 
            `USERGROUPS.WLST,USERGROUPS.SOF ` +
            `FROM ` +
            `(GROUPS, USERS, USERGROUPS) WHERE (USERS.UUID=USERGROUPS.UUID AND ` + 
            `GROUPS.UGID=USERGROUPS.UGID AND USERS.UUID='${req.session.user.uuid}')`
        db.all(
            query,
            [],
            function(err,rows) {
                //this gets all the groups which this user is a part of
                if(err)
                    throw err
                console.log(query,req.session.user.uuid,rows)
                for(let i=0;i<rows.length;i++) {
                    var dateObj = rows[i].EXP;
                    var date = parseInt(dateObj) - new Date()
                    rows[i].postDraw = date<=0;
                }
                res.render(path.join(__dirname,"views","home"),
                    {
                        group:rows
                    });
               
            }
        );
    } else {
        res.redirect("/");
    }
})
app.post("/update-list",(req,res) => {
    console.log(req.session.user);
    if(req.session.user) {//ensure errors do not happen
        var list = req.body.list;
        var uuid = req.session.user.uuid;
        var ugid = req.body.ugid;
        db.serialize(function(){
        db.run(`UPDATE USERGROUPS SET WLST='${list}' WHERE `+
            `UGID='${ugid}' AND UUID='${uuid}'`,[],function(err){
            console.log(uuid,ugid,list,"UPDATED!")
            if(err)
                throw err;
        })
        });
        res.redirect("/user");
    } else {
        res.redirect("/");
    }
})
app.post("/login",(req,res) => {
//check the login details with the database and allow login
    let query = `SELECT * FROM USERS WHERE EMAIL='${req.body.EMAIL}' AND PASS='${req.body.PASS}'`
    console.log(query);
    db.get(
        query,
        [],
        function (err,row) {
            if(err)
                throw err;
            if(row!=null) {
            console.log(req.body,row)
            var usr = {};
            usr.uuid = row.UUID;
            usr.email = row.EMAIL;
            usr.name = row.NAME;

            req.session.user = usr
            res.redirect("/user")
            } else {
                res.redirect("/")
            }
        }
    );
})

app.post("/create-group",function(req,res) {
    console.log(req.body)
    
    var group = req.body
    if(typeof(group.users)==typeof([])) {
        var g = {};
        g.$name = group.name;

        //first we will create the group which the users belong to using a uuid obj
        // :)
        var ugid = uuidv1()
        g.$ugid=ugid;
        let date = group.date.split("-");
        let t = group.time.split(":")
        let year = parseInt(date[0]);
        let month = parseInt(date[1]);
        let day = parseInt(date[2]);
        let hour = parseInt(t[0]);
        let minute = parseInt(t[1]);
        var d = new Date(year,month-1,day,hour,minute,0,0);
        g.$exp = d.valueOf()
        db.run(`INSERT INTO GROUPS (UGID, NAME, EXP,DRAWN) VALUES ($ugid,$name,$exp,0)`,g)

            for(let i=0;i<group.users.name.length;i++) {
                let usr = {};
                usr.$name = group.users.name[i]
                usr.$email = group.users.email[i]
                usr.$uuid = uuidv1()
                usr.$pass = uuidv1();
                db.serialize(function() {
                db.run(`INSERT OR IGNORE INTO USERS (UUID,NAME,EMAIL,PASS)` +
                `VALUES ('${usr.$uuid}','${usr.$name}','${usr.$email}','${usr.$pass}')`
                    )                      //retrieve the UUID which is on record not the current new one
                db.get(`SELECT * FROM USERS `+
                        `WHERE USERS.EMAIL='${usr.$email}';`,
                        [],(err,row)=>{
                                usr.$uuid=row.UUID;
                                console.log(`uuid: ${row.PASS} ${row.EMAIL}`)
                            }
                        )
                db.run(`INSERT INTO USERGROUPS ` + 
                              `(UGID,UUID,WLST,SOF) VALUES ` +
                              `('${ugid}','${usr.$uuid}','','');`
                        )
                });


            }
        res.redirect("/view-group?" + req.body.name);
    }
})

app.listen(8888);
